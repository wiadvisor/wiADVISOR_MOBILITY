Ext.define('wiAdvisorMobility.Locale',{
   override: 'Ext.Component',
    constructor: function(cfg) {
        if (cfg) {
            this.parser(cfg, 'title');
            this.parser(cfg, 'text');
            this.parser(cfg, 'labelFieldSet');
            this.parser(cfg, 'boxLabel');
            this.parser(cfg, 'fieldLabel');
            this.parser(cfg, 'label');
            this.parser(cfg, 'html');
        }
        
        this.callParent(arguments);
    },
    
    parser: function(config, property) {
        if (config[property]) {
            var text = config[property];
        
            if (text.substr(0, 1) == '#') {
                var result = localization[text.substr(1)];
            }
                
            config[property] = result||text;
        }
    }
});