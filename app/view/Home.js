/*
 * File: app/view/Home.js
 *
 * This file was generated by Sencha Architect
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.4.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.4.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('wiAdvisorMobility.view.Home', {
    extend: 'Ext.tab.Panel',

    requires: [
        'wiAdvisorMobility.view.wiMobCliente',
        'wiAdvisorMobility.view.wiMobCausali',
        'wiAdvisorMobility.view.wiMobAutorizzazioni',
        'Ext.tab.Bar'
    ],

    config: {
        activeItem: 2,
        id: 'mobilityTabPanelId',
        tabBar: {
            docked: 'top',
            id: 'mainTabBarID',
            itemId: 'mytabbar',
            ui: 'neutral',
            layout: {
                type: 'hbox',
                pack: 'center'
            },
            listeners: [
                {
                    fn: function(element, eOpts) {
                        if(!wiAdvisorMobility.isRefundable){
                            Ext.getCmp('mobilityTabPanelId').getTabBar().getAt(2).setTitle(localization.aut_no_refundable_title);
                        }
                    },
                    event: 'painted'
                }
            ]
        },
        items: [
            {
                xtype: 'wimobcliente',
                title: '#homeClienteVeicolo'
            },
            {
                xtype: 'wimobcausali',
                title: '#homeCausali'
            },
            {
                xtype: 'wimobautorizzazioni',
                title: '#homeAutorizzazioni'
            }
        ],
        listeners: [
            {
                fn: 'onHomePanelActiveItemChange',
                event: 'activeitemchange'
            }
        ]
    },

    /* gestisco i cambi di tab verificando che ci siano tutti i dati prima di procedere */
    onHomePanelActiveItemChange: function(container, value, oldValue, eOpts) {
        switch(this.getActiveItem().id){
            case 'customerVehicleTabID':
                //none
                break;
            case 'causaliTabID':
                //controllo cliente/veicolo
                if(!wiAdvisorMobility.currentCustomer || !wiAdvisorMobility.currentVehicle){
                    Ext.Msg.alert(localization.info, localization.insert_customer_vehicle_first,  function(button){
                        Ext.getCmp('mobilityTabPanelId').setActiveItem(0);
                    });
                    return;
                }else{
                    //controllo l'obbligatorietà dei campi
                    var errore = false;
                    if(wiAdvisorMobility.configuration.customer.NUMEROPATENTE == '2' && Ext.getCmp('homeContactDriverLicNumTxtFldId').getValue()==''){
                        errore = true;
                    }
                    if(wiAdvisorMobility.configuration.customer.SCADENZAPATENTE == '2' && Ext.getCmp('homeContactDriveLicExpireTxtFldId').getValue()==''){
                        errore = true;
                    }
                    if(wiAdvisorMobility.configuration.customer.NUMEROCARTAIDENTITA == '2' && Ext.getCmp('homeContactIdentificationNumTxtFldId').getValue()==''){
                        errore = true;
                    }
                    if(wiAdvisorMobility.configuration.customer.SCADENZACARTAIDENTITA == '2' && Ext.getCmp('homeContactDriverLicNumTxtFldId').getValue()==''){
                        errore = true;
                    }
                    if(errore){
                        Ext.Msg.alert(localization.info, localization.insert_customer_mandatory_first,  function(button){
                            Ext.getCmp('mobilityTabPanelId').setActiveItem(0);
                        });
                        return;
                    }
                }
                break;
            case 'authPanelID':
                //controllo cliente/veicolo
                if(!wiAdvisorMobility.currentCustomer || !wiAdvisorMobility.currentVehicle){
                    Ext.Msg.alert(localization.info, localization.insert_customer_vehicle_first,  function(button){
                        Ext.getCmp('mobilityTabPanelId').setActiveItem(0);
                    });
                    return;
                }else{
                    //controllo l'obbligatorietà dei campi
                    var errore = false;
                    if(wiAdvisorMobility.configuration.customer.NUMEROPATENTE == '2' && Ext.getCmp('homeContactDriverLicNumTxtFldId').getValue()==''){
                        errore = true;
                    }
                    if(wiAdvisorMobility.configuration.customer.SCADENZAPATENTE == '2' && Ext.getCmp('homeContactDriveLicExpireTxtFldId').getValue()==''){
                        errore = true;
                    }
                    if(wiAdvisorMobility.configuration.customer.NUMEROCARTAIDENTITA == '2' && Ext.getCmp('homeContactIdentificationNumTxtFldId').getValue()==''){
                        errore = true;
                    }
                    if(wiAdvisorMobility.configuration.customer.SCADENZACARTAIDENTITA == '2' && Ext.getCmp('homeContactDriverLicNumTxtFldId').getValue()==''){
                        errore = true;
                    }
                    if(errore){
                        Ext.Msg.alert(localization.info, localization.insert_customer_mandatory_first,  function(button){
                            Ext.getCmp('mobilityTabPanelId').setActiveItem(0);
                        });
                        return;
                    }
                }

                if(wiAdvisorMobility.isRefundable){
                    //controllo causali
                    var errore = false;
                    if(!Ext.getCmp('manodoperaSelectID').isChecked() && !Ext.getCmp('econtactSelectID').isChecked() && !Ext.getCmp('numOrdineSelectID').isChecked() && !Ext.getCmp('altroSelectID').isChecked() && !Ext.getCmp('incidenteSelectID').isChecked() && !Ext.getCmp('commSelectID').isChecked()){
                        errore = true;
                        Ext.Msg.alert(localization.info, localization.causali_mandatory,  function(button){
                            Ext.getCmp('mobilityTabPanelId').setActiveItem(1);
                        });
                        return;
                    }
                    //TR
                    if (Ext.getCmp('incidenteSelectID').isChecked()){
                        if(wiAdvisorMobility.Causali.TR[0]){
                            if(wiAdvisorMobility.Causali.TR[0].MANDATORY == '1' && Ext.getCmp('incidenteID').isChecked()){
                                errore = true;
                                if(errore){
                                    Ext.Msg.alert(localization.info, localization.causali_incidente_mandatory, function(button) {
                                        Ext.getCmp('mobilityTabPanelId').setActiveItem(1);
                                    });
                                    return;
                                }
                            }
                        }
                    }
                    //F fermo
                    if (Ext.getCmp('fermoSelectID').isChecked()){
                        if(wiAdvisorMobility.Causali.F[0]){
                            if(wiAdvisorMobility.Causali.F[0].MANDATORY == '1' && Ext.getCmp('fermoID').isChecked()){
                                errore = true;
                                if(errore){
                                    Ext.Msg.alert(localization.info, localization.causali_fermo_mandatory, function(button) {
                                        Ext.getCmp('mobilityTabPanelId').setActiveItem(1);
                                    });
                                    return;
                                }
                            }
                        }
                    }
                    //TH altro
                    if (Ext.getCmp('altroSelectID').isChecked()){
                        if(wiAdvisorMobility.Causali.TH[0]){
                            if(wiAdvisorMobility.Causali.TH[0].MANDATORY == '1' && Ext.getCmp('altroID').isChecked()){
                                errore = true;
                                if(errore){
                                    Ext.Msg.alert(localization.info, localization.causali_altro_mandatory, function(button) {
                                        Ext.getCmp('mobilityTabPanelId').setActiveItem(1);
                                    });
                                    return;
                                }
                            }
                        }
                    }
                    //GC gestione commerciale
                    if (Ext.getCmp('commSelectID').isChecked()){
                        if(wiAdvisorMobility.Causali.GC[0]){
                            if(wiAdvisorMobility.Causali.GC[0].MANDATORY == '1' && Ext.getCmp('commID').isChecked()==''){
                                errore = true;
                                if(errore){
                                    Ext.Msg.alert(localization.info, localization.causali_comm_mandatory, function(button) {
                                        Ext.getCmp('mobilityTabPanelId').setActiveItem(1);
                                    });
                                    return;
                                }
                            }
                        }
                    }
                    //T econtact
                    if (Ext.getCmp('econtactSelectID').isChecked()){
                        if(wiAdvisorMobility.Causali.T[0]){
                            if(wiAdvisorMobility.Causali.T[0].MANDATORY == '1' && Ext.getCmp('econtactID').getValue() == ''){
                                Ext.Msg.alert(localization.info, localization.causali_econtact_mandatory, function(button) {
                                    Ext.getCmp('mobilityTabPanelId').setActiveItem(1);
                                });
                                return;
                                errore = true;
                            }
                            if(wiAdvisorMobility.Causali.T[1].MANDATORY == '1' && (Ext.getCmp('categoriaID').getValue() == '' || Ext.getCmp('categoriaID').getValue() == null)){
                                Ext.Msg.alert(localization.info, localization.causali_categori_mandatory, function(button) {
                                    Ext.getCmp('mobilityTabPanelId').setActiveItem(1);
                                });
                                return;
                                errore = true;
                            }
                            wiAdvisorMobility.app.getController('Main').showLoadingMask(localization.loading);
                            wiAdvisorMobility.app.getController('Main').checkTicketEContact(Ext.getCmp('econtactID').getValue(), Ext.getCmp('categoriaID').getValue());
                        }
                    }
                    //M manodopera
                    if (Ext.getCmp('manodoperaSelectID').isChecked()){
                        if(wiAdvisorMobility.Causali.M[0]){
                            if(wiAdvisorMobility.Causali.M[0].MANDATORY == '1' && Ext.getCmp('tempoManodoperaID').getValue() == ''){
                                Ext.Msg.alert(localization.info, localization.causali_manodopera_mandatory, function(button) {
                                    Ext.getCmp('mobilityTabPanelId').setActiveItem(1);
                                });
                                return;
                                errore = true;
                            }
                            if(wiAdvisorMobility.Causali.M[1].MANDATORY == '1' && Ext.getCmp('inconvenienteID').getValue() == ''){
                                Ext.Msg.alert(localization.info, localization.causali_manodopera_mandatory, function(button) {
                                    Ext.getCmp('mobilityTabPanelId').setActiveItem(1);
                                });
                                return;
                                errore = true;
                            }
                        }
                    }
                    //R numero ordine
                    if (Ext.getCmp('numOrdineSelectID').isChecked()){
                        if(wiAdvisorMobility.Causali.R[0]){
                            if(wiAdvisorMobility.Causali.R[0].MANDATORY == '1' && Ext.getCmp('numOrdineID').getValue() == ''){
                                errore = true;
                                Ext.Msg.alert(localization.info, localization.causali_numordine_mandatory, function(button) {
                                    Ext.getCmp('mobilityTabPanelId').setActiveItem(1);
                                });
                                return;
                            }
                            if(wiAdvisorMobility.Causali.R[1].MANDATORY == '1' && Ext.getCmp('dealerCodeID').getValue() == ''){
                                errore = true;
                                Ext.Msg.alert(localization.errinfoore, localization.causali_numordine_mandatory, function(button) {
                                    Ext.getCmp('mobilityTabPanelId').setActiveItem(1);
                                });
                                return;
                            }
                        }
                    }
                    if(!errore){
                        var codCausale = '';
                        codCausale = Ext.getCmp('manodoperaSelectID').isChecked() ? 'M' : Ext.getCmp('econtactSelectID').isChecked() ? 'T' : Ext.getCmp('numOrdineSelectID').isChecked() ? 'R' : 'TH';
                        wiAdvisorMobility.app.getController('Main').checkDealerParameters(codCausale);
                    }
                }else{
                    //setto la data di restituzione
                    var numDays = wiAdvisorMobility.app.getController('Main').getDeliveryNumDays();
                    Ext.getCmp('dataRestituzioneID').setReadOnly(false);
                    Ext.getCmp('dataRestituzioneID').setOptions(numDays);
                }
                break;
        }
    }

});