/*
 * File: app/store/storeDeliveryTypes.js
 *
 * This file was generated by Sencha Architect
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.4.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.4.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('wiAdvisorMobility.store.storeDeliveryTypes', {
    extend: 'Ext.data.Store',

    requires: [
        'wiAdvisorMobility.model.storeDeliveryTypes',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    config: {
        autoLoad: true,
        model: 'wiAdvisorMobility.model.storeDeliveryTypes',
        params: {
            action: 'getAuthDeliveries'
        },
        storeId: 'storeDeliveryTypes',
        proxy: {
            type: 'ajax',
            url: '/appl/Mobility/richiediVettura/cvAuth.php',
            reader: {
                type: 'json'
            }
        }
    }
});