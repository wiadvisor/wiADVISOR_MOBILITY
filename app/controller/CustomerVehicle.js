/*
 * File: app/controller/CustomerVehicle.js
 *
 * This file was generated by Sencha Architect
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.4.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.4.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('wiAdvisorMobility.controller.CustomerVehicle', {
    extend: 'Ext.app.Controller',

    requires: [
        'Ext.form.DatePicker',
        'Ext.picker.Date'
    ],

    config: {
    },

    /* metto a video i dettagli del cliente */
    showSchedaContactData: function() {
        Ext.getCmp('homeContactLastNameTxtFldId').setValue(wiAdvisorMobility.currentCustomer.data.CONTACTNAME);
        Ext.getCmp('homeContactFirstNameTxtFldId').setValue(wiAdvisorMobility.currentCustomer.data.FIRSTNAME);
        Ext.getCmp('homeContactPhoneTxtFldId').setValue(wiAdvisorMobility.currentCustomer.data.TELEPHONE);
        Ext.getCmp('homeContactCellTxtFldId').setValue(wiAdvisorMobility.currentCustomer.data.CELULAR);
        Ext.getCmp('homeContactDriverLicNumTxtFldId').setValue(wiAdvisorMobility.currentCustomer.data.NUMEROPATENTE);
        Ext.getCmp('homeContactIdentificationNumTxtFldId').setValue(wiAdvisorMobility.currentCustomer.data.NUMEROCARTAIDENTITA);
        if(wiAdvisorMobility.currentCustomer.data.SCADENZAPATENTE && wiAdvisorMobility.currentCustomer.data.SCADENZAPATENTE!=''){
            var scadPatente = wiAdvisorMobility.app.getController('CustomerVehicle').formatDate(wiAdvisorMobility.currentCustomer.data.SCADENZAPATENTE);
            Ext.getCmp('homeContactDriveLicExpireTxtFldId').setValue(Ext.DateExtras.format(new Date(scadPatente),locale_dateformat['default']));
        }
        if(wiAdvisorMobility.currentCustomer.data.SCADENZACARTAIDENTITA && wiAdvisorMobility.currentCustomer.data.SCADENZACARTAIDENTITA!=''){
            var scadIdentita = wiAdvisorMobility.app.getController('CustomerVehicle').formatDate(wiAdvisorMobility.currentCustomer.data.SCADENZACARTAIDENTITA);
            Ext.getCmp('homeContactIdentificationExpTxtFldId').setValue(Ext.DateExtras.format(new Date(scadIdentita),locale_dateformat['default']));
        }
        document.getElementById('lblVehicleBoxID').innerHTML = localization.cart_vehicle_customer;
        document.getElementById('cartCustomerTxtFldId').innerHTML = wiAdvisorMobility.currentCustomer.data.CONTACTNAME+' '+wiAdvisorMobility.currentCustomer.data.FIRSTNAME;
    },

    /* metto a video i dettagli del veicolo */
    showSchedaVehicleData: function() {
        Ext.getCmp('homeVehiclePlateTxtFldId').setValue(wiAdvisorMobility.currentVehicle.data.PLATE);
        Ext.getCmp('homeVehicleVinTxtFldId').setValue(wiAdvisorMobility.currentVehicle.data.VIN);
        Ext.getCmp('homeVehicleYearTxtFldId').setValue(wiAdvisorMobility.currentVehicle.data.MODELYEAR);
        Ext.getCmp('homeVehicleDescrTxtFldId').setValue(wiAdvisorMobility.currentVehicle.data.VEHICLENAME+' '+wiAdvisorMobility.currentVehicle.data.VEHICLEVERSION);
        if (wiAdvisorMobility.currentVehicle.data.WARRANTYDATE!== '' && wiAdvisorMobility.currentVehicle.data.WARRANTYDATE!== null ) {
            var d = new Date(Utils.ParseDate(wiAdvisorMobility.currentVehicle.data.WARRANTYDATE));
            Ext.getCmp('homeVehicleWarrantyTxtFldId').setValue(Ext.DateExtras.format(d,locale_dateformat['default']));
        }
        Ext.getCmp('homeVehicleKmTxtFldId').setValue(wiAdvisorMobility.currentVehicle.data.KM);
        Ext.getCmp('homeVehicleExtColorTxtFldId').setValue(wiAdvisorMobility.currentVehicle.data.DESCRCOLOR);
        var assignedMkt = '';
        if(wiAdvisorMobility.currentVehicle.data.MERCATOVENDITA){
            assignedMkt += wiAdvisorMobility.currentVehicle.data.MERCATOVENDITA;
        }
        if(wiAdvisorMobility.currentVehicle.data.DESCRMERCATOVENDITA){
            assignedMkt += ' ' +wiAdvisorMobility.currentVehicle.data.DESCRMERCATOVENDITA;
        }
        Ext.getCmp('homeVehicleMercatoAssTxtFldId').setValue(assignedMkt);

        document.getElementById('cartVehicleTxtFldId').innerHTML = wiAdvisorMobility.currentVehicle.data.VEHICLENAME+' '+wiAdvisorMobility.currentVehicle.data.VEHICLEVERSION;
    },

    /* metto a video i dettagli del veicolo generico */
    showSchedaGenericVehicleData: function() {
        Ext.getCmp('homeVehiclePlateTxtFldId').setValue(wiAdvisorMobility.repairorderBeanInfo.data.PLATE_GENERIC);
        Ext.getCmp('homeVehicleDescrTxtFldId').setValue(wiAdvisorMobility.repairorderBeanInfo.data.VEHICLEDESCRIPTION);
        Ext.getCmp('homeVehicleKmTxtFldId').setValue(wiAdvisorMobility.repairorderBeanInfo.data.KMVEHICLE);

        document.getElementById('cartVehicleTxtFldId').innerHTML = wiAdvisorMobility.repairorderBeanInfo.data.VEHICLEDESCRIPTION;
    },

    /* overlay di dettaglio cliente */
    openCustomerPanel: function() {
        // apro solo se già selezionato
        if(typeof wiAdvisorMobility.currentCustomer != 'undefined') {
            var dataPatente = '';
            var dataCarta = '';
            if(wiAdvisorMobility.currentCustomer.data.SCADENZAPATENTE){
                var patTmp = wiAdvisorMobility.app.getController('CustomerVehicle').formatDate(wiAdvisorMobility.currentCustomer.data.SCADENZAPATENTE);
                dataPatente = new Date(patTmp);
            }
            if(wiAdvisorMobility.currentCustomer.data.SCADENZACARTAIDENTITA){
                var idTmp = wiAdvisorMobility.app.getController('CustomerVehicle').formatDate(wiAdvisorMobility.currentCustomer.data.SCADENZACARTAIDENTITA);
                dataCarta = new Date(idTmp);
            }

            var customerOverlay = Ext.create('Ext.Panel', {
                modal: true,
                centered: true,
                width: 600,
                height: 560,
                id:'customerContactPopUpId',
                zIndex: 10,
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'titlebar',
                        docked: 'top',
                        id: 'contactToolbarId',
                        title: localization.txt_contact,
                        items: [
                            {
                                xtype: 'button',
                                align: 'right',
                                id: 'SaveCustomerId',
                                iconCls: 'save',
                                handler: function() {
                                    wiAdvisorMobility.app.getController('CustomerVehicle').checkBeforeSaveCustomer();
                                }
                            },
                            {
                                xtype: 'button',
                                align: 'right',
                                id: 'CloseCustomerId',
                                iconCls: 'delete',
                                handler: function() {
                                    customerOverlay.destroy();
                                }
                            }
                        ]
                    },//END TITLEBAR
                    {
                        xtype: 'formpanel',
                        height: 530,
                        scrollable: 'vertical',
                        id: 'contactFormId',
                        layout: {
                            align: 'center'
                        },
                        items: [
                            {
                                xtype: 'fieldset',
                                margin: '10 15 5 15',
                                padding: 0,
                                items: [
                                    {
                                        xtype: 'image',
                                        docked: 'top',
                                        height: 86,
                                        width: 150,
                                        align: 'center',
                                        src: '/attachments/images/SRM/user_color.png'
                                    },
                                    {
                                        xtype: 'spacer'
                                    },
                                    {
                                        xtype: 'textfield',
                                        id: 'contactDetContactnameId',
                                        label: localization.cognome,
                                        name: 'CONTACTNAME',
                                        readOnly: true,
                                        value: wiAdvisorMobility.currentCustomer.data.CONTACTNAME
                                    },
                                    {
                                        xtype: 'textfield',
                                        id:		'contactDetFirstnameId',
                                        label: localization.nome,
                                        name:  'FIRSTNAME',
                                        readOnly: true,
                                        value: wiAdvisorMobility.currentCustomer.data.FIRSTNAME
                                    },
                                    {
                                        xtype: 'textfield',
                                        id: 'contactDetTelephoneId',
                                        label: localization.telefono,
                                        name:  'TELEPHONE',
                                        readOnly: true,
                                        value: wiAdvisorMobility.currentCustomer.data.TELEPHONE
                                    },
                                    {
                                        xtype: 'textfield',
                                        id:	'contactDetCelularId',
                                        label: localization.cellulare,
                                        name: 'CELULAR',
                                        readOnly: true,
                                        value: wiAdvisorMobility.currentCustomer.data.CELULAR
                                    }
                                ]
                             },
                             {
                                xtype: 'fieldset',
                                margin: '10 15 5 15',
                                padding: 0,
                                items: [
                                    //campi in inserimento parametrici
                                    {
                                        xtype: 'textfield',
                                        id:	'contactDetNumPatenteId',
                                        label: localization.numpatente,
                                        value: wiAdvisorMobility.currentCustomer.data.NUMEROPATENTE,
                                        hidden: (wiAdvisorMobility.configuration.customer.NUMEROPATENTE == '0')?true:false,
                                        required: (wiAdvisorMobility.configuration.customer.NUMEROPATENTE == '2')?true:false
                                    },
                                    {
                                        xtype: 'datepickerfield',
                                        picker: {
                                            xtype: 'datepicker',
                                            autoSelect: true,
                                            slotOrder: locale_slots,
                                            yearFrom: new Date().getFullYear(),
                                            yearTo: new Date().getFullYear() + 10,
                                            cancelButton: {
                                                text: localization.picker_cancel
                                            },
                                            doneButton: {
                                                text: localization.picker_done
                                            },
                                            toolbar: {
                                                items: [
                                                    {
                                                        xtype: 'button',
                                                        text: localization.btn_clear,
                                                        id: 'clearDataScadPatenteID',
                                                        handler: function(button, event) {
                                                            var picker = button.up('datepicker');
                                                            picker.fireEvent('change', picker, null);
                                                            picker.hide();
                                                        },
                                                        disabled: wiAdvisorMobility.configuration.customer.SCADENZAPATENTE == '2'
                                                    }
                                                ]
                                            }
                                        },
                                        dateFormat: locale_dateformat['default'],
                                        id:	'contactDetScadPatenteId',
                                        label: localization.scadpatente,
                                        value: dataPatente,
                                        hidden: (wiAdvisorMobility.configuration.customer.SCADENZAPATENTE == '0')?true:false,
                                        required: (wiAdvisorMobility.configuration.customer.SCADENZAPATENTE == '2')?true:false
                                    },
                                    {
                                        xtype: 'textfield',
                                        id:	'contactDetNumCartaId',
                                        label: localization.numcartaid,
                                        value: wiAdvisorMobility.currentCustomer.data.NUMEROCARTAIDENTITA,
                                        hidden: (wiAdvisorMobility.configuration.customer.NUMEROCARTAIDENTITA == '0')?true:false,
                                        required: (wiAdvisorMobility.configuration.customer.NUMEROCARTAIDENTITA == '2')?true:false
                                    },
                                    {
                                        xtype: 'datepickerfield',
                                        picker: {
                                            xtype: 'datepicker',
                                            autoSelect: true,
                                            slotOrder: locale_slots,
                                            yearFrom: new Date().getFullYear(),
                                            yearTo: new Date().getFullYear() + 10,
                                            cancelButton: {
                                                text: localization.picker_cancel
                                            },
                                            doneButton: {
                                                text: localization.picker_done
                                            },
                                            toolbar: {
                                                items: [
                                                    {
                                                        xtype: 'button',
                                                        text: localization.btn_clear,
                                                        id: 'clearDataScadCartaID',
                                                        handler: function(button, event) {
                                                            var picker = button.up('datepicker');
                                                            picker.fireEvent('change', picker, null);
                                                            picker.hide();
                                                        },
                                                        disabled: wiAdvisorMobility.configuration.customer.SCADENZACARTAIDENTITA == '2'
                                                    }
                                                ]
                                            }
                                        },

                                        dateFormat: locale_dateformat['default'],
                                        id:	'contactDetScadCartaId',
                                        label: localization.scadcartaid,
                                        value: dataCarta,
                                        hidden: (wiAdvisorMobility.configuration.customer.SCADENZACARTAIDENTITA == '0')?true:false,
                                        required: (wiAdvisorMobility.configuration.customer.SCADENZACARTAIDENTITA == '2')?true:false
                                    }
                                ]
                            }
                        ] //end panel items
                    }//end formpanel
                ]
            });

            //metto a video
            Ext.Viewport.add(customerOverlay);
            customerOverlay.show();
        }
    },

    /* salvataggio dati cliente */
    saveCustomer: function() {
        wiAdvisorMobility.app.getController('Main').showLoadingMask(localization.loading);
        //date
        var dataPatente = '';
        var dataCarta = '';
        if(Ext.getCmp('contactDetScadPatenteId').getValue() && Ext.getCmp('contactDetScadPatenteId').getValue()!=''){
            dataPatente = new Date(Ext.getCmp('contactDetScadPatenteId').getValue());
            dataPatente = Ext.DateExtras.format(dataPatente,'d/m/Y');
        }
        if(Ext.getCmp('contactDetScadCartaId').getValue() && Ext.getCmp('contactDetScadCartaId').getValue()!=''){
            dataCarta = new Date(Ext.getCmp('contactDetScadCartaId').getValue());
            dataCarta = Ext.DateExtras.format(dataCarta,'d/m/Y');
        }

        Ext.Ajax.request({
            url: '/appl/wiAdvisorMobility/index.php',
            params: {
                entry: 'storeDocumentData',
                params: {
                    params: Ext.JSON.encode({
                        codcontact: wiAdvisorMobility.currentCustomer.data.CODCONTACT,
                        numeropatente: Ext.getCmp('contactDetNumPatenteId').getValue(),
                        scadenzapatente: dataPatente,
                        numerocartaidentita: Ext.getCmp('contactDetNumCartaId').getValue(),
                        scadenzacartaidentita: dataCarta
                    })
                }
            },
            method: 'POST',
            success: function(response, request) {
                var result = Ext.JSON.decode(response.responseText);
                if(result.success == 'true'){
                    var message = localization.save_ok;
                    Ext.getCmp('homeContactDriverLicNumTxtFldId').setValue(Ext.getCmp('contactDetNumPatenteId').getValue());
                    Ext.getCmp('homeContactDriveLicExpireTxtFldId').setValue(dataPatente);
                    Ext.getCmp('homeContactIdentificationNumTxtFldId').setValue(Ext.getCmp('contactDetNumCartaId').getValue());
                    Ext.getCmp('homeContactIdentificationExpTxtFldId').setValue(dataCarta);
                    //riporto nell'oggetto customer
                    wiAdvisorMobility.currentCustomer.data.NUMEROPATENTE = Ext.getCmp('contactDetNumPatenteId').getValue();
                    wiAdvisorMobility.currentCustomer.data.SCADENZAPATENTE = dataPatente;
                    wiAdvisorMobility.currentCustomer.data.NUMEROCARTAIDENTITA = Ext.getCmp('contactDetNumCartaId').getValue();
                    wiAdvisorMobility.currentCustomer.data.SCADENZACARTAIDENTITA = dataCarta;
                }else{
                    var message = localization.save_ko;
                }
                wiAdvisorMobility.app.getController('Main').hideLoadingMask();
                wiAdvisorMobility.app.getController('Main').showTimeoutMessageBox(localization.informzione,message);

                //chiudo l'overlay customer
                Ext.getCmp('customerContactPopUpId').destroy();
            }
        });
    },

    loadCustomerVehicle: function() {
        wiAdvisorMobility.app.getController('Main').showLoadingMask(localization.loading);

        //warranty
        wiAdvisorMobility.Warranty = {};
        wiAdvisorMobility.Warranty.codPolicy = '';
        wiAdvisorMobility.Warranty.warrantyType = '';
        wiAdvisorMobility.Warranty.extWarrantyContractStartDate = '';

        //carico lamentati della commessa
        var store = Ext.getStore('Faults');
        store.load({
            params: {
                CodRepairOrder: wiAdvisorMobility.codRepairOrder
            }
        });

        Ext.getStore('Repairorder').load({
            params: {
                "codRepairOrder": wiAdvisorMobility.codRepairOrder
            },
            callback: function() {
                if (Ext.getStore('Repairorder').data.length > 0) {
                    wiAdvisorMobility.repairorderBeanInfo = Ext.getStore("Repairorder").data.items['0'];
                    //OTTENGO DATI customer e setto scheda cliente
                    var codcontact = Ext.getStore('Repairorder').data.items[0].data.CODCONTACT;
                    Ext.Ajax.request({
                        url: '/appl/SRM/index.php',
                        params: {
                            entry: 'ajaxRequest',
                            module: 'getContactInfo',
                            params: {
                                params: Ext.JSON.encode({
                                    codcontact: codcontact,
                                    custiddms: ''
                                })
                            }
                        },
                        method: 'POST',
                        success: function(response, request) {
                            var resultObj = response.responseText;
                            var ObjDecoded = Ext.JSON.decode(resultObj);

                            //OTTENGO DATI veicolo e setto scheda cliente
                            var codvehicle = Ext.getStore('Repairorder').data.items[0].data.CODVEHICLE;
                            Ext.getStore('ContactInfo').add(ObjDecoded.result);
                            wiAdvisorMobility.currentCustomer = Ext.getStore("ContactInfo").getAt(0);

                            //ottengo i campi specifici di mobility da aggiungere al contatto
                            Ext.Ajax.request({
                                url: '/appl/wiAdvisorMobility/index.php',
                                params: {
                                    entry: 'loadDocumentData',
                                    params: {
                                        params: Ext.JSON.encode({
                                            codcontact: codcontact
                                        })
                                    }
                                },
                                success: function(response, request) {
                                    //metto il json di ritorno nell'oggetto currentCustomer
                                    var mobData = Ext.JSON.decode(response.responseText);
                                    wiAdvisorMobility.currentCustomer.data.NUMEROPATENTE = mobData.numeropatente;
                                    wiAdvisorMobility.currentCustomer.data.SCADENZAPATENTE = mobData.scadenzapatente;
                                    wiAdvisorMobility.currentCustomer.data.NUMEROCARTAIDENTITA = mobData.numerocartaidentita;
                                    wiAdvisorMobility.currentCustomer.data.SCADENZACARTAIDENTITA = mobData.scadenzacartaidentita;

                                    //ottengo la configurazione dei campi specifici del contatto
                                    /*
                                        ["2","0","0","0"]

                                        2 : visibile e mandatorio
                                        1 : visibile e non mandatorio
                                        0 : nascosto

                                        l'ordine è : numero patente, scadenza patente, numero CI, scadenza CI
                                    */
                                    Ext.Ajax.request({
                                        url: '/appl/wiAdvisorMobility/index.php',
                                        params: {
                                            action: 'getPatenteCartaIdVisible',
                                        },
                                        success: function(response, request) {
                                            //metto il json di ritorno nell'oggetto currentCustomer
                                            var configDocs = Ext.JSON.decode(response.responseText);
                                            wiAdvisorMobility.configuration = {};
                                            wiAdvisorMobility.configuration.customer = {};
                                            wiAdvisorMobility.configuration.customer.NUMEROPATENTE = configDocs[0];
                                            wiAdvisorMobility.configuration.customer.SCADENZAPATENTE = configDocs[1];
                                            wiAdvisorMobility.configuration.customer.NUMEROCARTAIDENTITA = configDocs[2];
                                            wiAdvisorMobility.configuration.customer.SCADENZACARTAIDENTITA = configDocs[3];

                                            //abilito i campi in home e metto i dati
                                            wiAdvisorMobility.app.getController('Main').setUserFields();
                                            wiAdvisorMobility.app.getController('CustomerVehicle').showSchedaContactData();
                                            wiAdvisorMobility.app.getController('Main').showRepairOrderData(Ext.getStore('Repairorder').data.items[0].data);

                                            if (codvehicle !== '' && codvehicle !== null) {
                                                paramsArrayVeic = {
                                                    "codvehicle": codvehicle
                                                };
                                                Ext.getStore('VehicleInfo').load({
                                                    params: paramsArrayVeic,
                                                    callback: function() {
                                                        // set info customer nel namespace
                                                        wiAdvisorMobility.currentVehicle = Ext.getStore("VehicleInfo").getById(codvehicle);
                                                        // set dati del veicolo nella scheda
                                                        wiAdvisorMobility.app.getController('CustomerVehicle').showSchedaVehicleData();
                                                        //precaricola struttra delle causali
                                                        wiAdvisorMobility.app.getController('Main').setUpApplication();
                                                    }
                                                });
                                            } else {
                                                wiAdvisorMobility.app.getController('Main').hideLoadingMask(true);
                                                wiAdvisorMobility.currentVehicle = Ext.create('wiAdvisorMobility.model.VehicleInfo', {
                                                    GENERIC_VEHICLE: true,
                                                    CODVEHICLE: 'null',
                                                    //questo campo lo devo inizializzare a qualche cosa e poi metterlo ad undefined fuori da sta create, sennò si prende un fakeid
                                                    PLATE: wiAdvisorMobility.repairorderBeanInfo.data.PLATE_GENERIC,
                                                    VEHICLENAME: '',
                                                    VEHICLEVERSION: wiAdvisorMobility.repairorderBeanInfo.data.VEHICLEDESCRIPTION,
                                                    KM: wiAdvisorMobility.repairorderBeanInfo.data.KMVEHICLE,
                                                    CHASSIS: '',
                                                    WARRANTYDATE: ''
                                                });
                                                wiAdvisorMobility.currentVehicle.data.CODVEHICLE = undefined;
                                                // set dati del veicolo nella scheda
                                                wiAdvisorMobility.app.getController('CustomerVehicle').showSchedaGenericVehicleData();
                                            }
                                        }//end succes config docs
                                    });
                                }//end success docsdata per contatto
                            });
                        } // end succes load contactInfo
                    }); //end contactinfo
                } else {
                    wiAdvisorMobility.app.getController('Main').hideLoadingMask(true);
                    Ext.Msg.alert(localization.info,localization.repairorder_not_found, Ext.emptyFn);
                }
            }
        });
    },

    checkBeforeSaveCustomer: function() {
        //controllo l'obbligatorietà dei campi
        var errore = false;
        if(wiAdvisorMobility.configuration.customer.NUMEROPATENTE == '2' && Ext.getCmp('contactDetNumPatenteId').getValue()==''){
            errore = true;
        }
        if(wiAdvisorMobility.configuration.customer.SCADENZAPATENTE == '2' && Ext.getCmp('contactDetScadPatenteId').getValue()==''){
            errore = true;
        }
        if(wiAdvisorMobility.configuration.customer.NUMEROCARTAIDENTITA == '2' && Ext.getCmp('contactDetNumCartaId').getValue()==''){
            errore = true;
        }
        if(wiAdvisorMobility.configuration.customer.SCADENZACARTAIDENTITA == '2' && Ext.getCmp('contactDetScadCartaId').getValue()==''){
            errore = true;
        }
        if(!errore){
            wiAdvisorMobility.app.getController('CustomerVehicle').saveCustomer();
        }else{
            Ext.Msg.alert(localization.info, localization.customer_form_mandatory, Ext.emptyFn);
        }
    },

    /*
        formatto in locale la data e ritorno il tipo corretto
        dal db mi arriva sempre il formato d/m/Y
    */
    formatDate: function(myDate) {
        var arrDate = myDate.split('/');
        var day = arrDate[0];
        var month = arrDate[1];
        var year = arrDate[2];
        var dbDate = month+'/'+day+'/'+year;

        return dbDate;
    }

});